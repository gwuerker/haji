import logging

import tornado.ioloop
import tornado.web

from handler import HomeHandler, UserHandler

class HajiLandingApplication(tornado.web.Application):
	def __init__(self):
		tornado.web.Application.__init__(self)	
		self.add_handlers('', [
			(r'/', HomeHandler),
			(r'/user/([^/]*)', UserHandler),
			(r'/js/(.*)', tornado.web.StaticFileHandler, {'path': './landing/javascript/'}),
		])


	def run(self):
		self.listen(52031)
		tornado.ioloop.IOLoop.instance().start()



