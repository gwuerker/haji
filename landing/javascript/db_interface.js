function getJobs(search, limit){
	var response = $.ajax({
		type: "GET",
		url: 'http://bgsc.uwec.edu:52030/' + search + '?limit=' + limit,
		async: false
	}).responseText;
	return JSON.parse(response);
}

function pullUsers(jobs){
	var users = [];
	$.each(jobs, function(key, job){
		user = job['user'];
		if($.inArray(user, users) === -1){
			users.push(user);
		}
	});
	return users;
}

function userData(jobs){
	var data = { 
		'headers' : [
			{
				'key' : 'user', 
				'name' : 'User Name',
				'type' : 'a',
				'attrs' : ['href']
			}, 
			{
				'key' : 'efficiency', 
				'name' : 'Avg. Efficiency',
				'type' : 'font',
				'attrs' : ['color']
			},
			{
				'key' : 'weight', 
				'name' : 'Weight',
				'type' : 'span',
				'attrs' : []
			}
		], 
		'values' : [] 
	}
			
	var users = pullUsers(jobs);
	$.each(users, function(key, user){	
		var userValues = {
			'user' : {
				'html' : user,
				'attrs' : {
					'href' : user
				}
			},
			'efficiency' : {
				'html' : getUserEff(jobs, user, true),
				'attrs' : {
					'color' : getEffColor(getUserEff(jobs, user, false))
				}
			},
			'weight' : {
				'html' : getUserWeight(jobs, user, true),
				'attrs' : {}
			}
		};
		data['values'].push(userValues);
	});

	return data;
}

function jobData(jobs){
	var data = { 
		'headers' : [
			{
				'key' : 'job_id',
				'name' : 'Job ID',
				'type' : 'span',
				'attrs' : []
			},
			{
				'key' : 'job_name', 
				'name' : 'Job Name',
				'type' : 'span',
				'attrs' : []
			},
			{
				'key' : 'efficiency',
				'name' : 'Efficiency',
				'type' : 'font',
				'attrs' : ['color']
			},
			{
				'key' : 'run_time',
				'name' : 'Run Time',
				'type' : 'span',
				'attrs' : []
			},
			{
				'key' : 'procs_used',
				'name' : 'Processors',
				'type' : 'span',
				'attrs' : []
			},
			{
				'key' : 'start_time',
				'name' : 'Start Time',
				'type' : 'span',
				'attrs' : []
			}
		], 
		'values' : []
	}
			
	$.each(jobs, function(key, job){	
		var jobValues = {
			'job_id' : {
				'html' : job['torque_id'],
				'attrs' : {}
			},
			'job_name' : {
				'html' : job['job_name'],
				'attrs' : {}
			},  
			'efficiency' : {
				'html' : getJobEff(job, true),
				'attrs' : {
					'color' : getEffColor(getJobEff(job, false))
				}
			},
			'run_time' : {
				'html' : job['used_resources']['walltime'],
				'attrs' : {}
			},
			'procs_used' : {
				'html' : getNumProcs(job),
				'attrs' : {}
			},
			'start_time' : {
				'html' : $.timeago(1000 * parseInt(job['start_time'])),
				'attrs' : {}
			}

		};
		data['values'].push(jobValues);
	});

	return data;
}


function getUserEff(jobs, user, pretty){
	var efficiencySum = 0;
	var weightSum = 0;
	$.each(jobs, function(key, job){
		if(job['user'] == user){
			var weight = getRunTime(job) * getNumProcs(job);
			efficiencySum += getJobEff(job, false) * weight;
			weightSum += weight;
		}
	});

	if(weightSum == 0){
		var efficiency = 1;
	}else{
		var efficiency = efficiencySum / weightSum;
	}

	if(pretty)
		return Math.round(efficiency * 100) + '%';

	return efficiency

}

function getUserWeight(jobs, user, pretty){
	var userWeight = 0;
	var totalWeight = 0;

	$.each(jobs, function(key, job){
		var jobWeight = getNumProcs(job) * getRunTime(job);
		if(user == job['user']){
			userWeight += jobWeight;
		}
		
		totalWeight += jobWeight;
	});
	
	if(totalWeight == 0){
		var userWeightPercent = 1;
	}else{
		var userWeightPercent = userWeight / totalWeight;
	}

	console.log(user + ': ' + userWeight + ' / ' + totalWeight);
	
	if(pretty)
		return Math.round(userWeightPercent * 100) + '%'

	return userWeightPercent;

}

function getJobEff(job, pretty){	
	var efficiency = getCPUTime(job) / (getRunTime(job) * getNumProcs(job))

	if(efficiency > 1 || getRunTime(job) == 0)
		efficiency = 1

	if(pretty)
		return Math.round(efficiency * 100) + '%';

	return efficiency
}

function getEffColor(efficiency){
	efficiency3 = Math.pow(efficiency, 3);

	var red = Math.round((1 - efficiency3) * 300);
	var green = Math.round(efficiency3 * 300);
	var blue = 0;

	if(red > 150){
		red = 150
	}else if(green > 150){
		green = 150;
	}

	return '#' + componentToHex(red)  + componentToHex(green)  + componentToHex(blue);
}

function componentToHex(num) {
    var hex = num.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function getRunTime(job){
	var walltString = job['used_resources']['walltime'];
	var walltSplit = walltString.split(':');

	return parseInt(walltSplit[0]) * 3600 + parseInt(walltSplit[1]) * 60 + parseInt(walltSplit[2])
}

function getNumProcs(job){
	var nodes = job['exec_host'];
	var nodesSplit = nodes.split('+');

	return nodesSplit.length;
}

function getCPUTime(job){
	var cputString = job['used_resources']['cput'];
	var cputSplit = cputString.split(':');

	return parseInt(cputSplit[0]) * 3600 + parseInt(cputSplit[1]) * 60 + parseInt(cputSplit[2])
}
