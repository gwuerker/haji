function dataToTable(table, data){
	var html = '';
	html += '<thead>'
	$.each(data['headers'], function(key, header){
		html += '<th>' + header['name']  + '</th>'
	});
	html += '</thead>'

	html += '<tbody>'
	$.each(data['values'], function(value_key, value){
		html += '<tr>'
		$.each(data['headers'], function(header_key, header){
			html += '<td>';
			html += '<' + header['type']
			$.each(header['attrs'], function(attr_key, attr){
				html += ' ' + attr + '="' + value[header['key']]['attrs'][attr] + '"';
			});
			html += '>';
			html += value[header['key']]['html'];
			html += '</' + header['type'] + '>';
			html += '</td>';
		});
		html += '</tr>';
	});
	table.html(html);
	html += '</tbody>';
}
