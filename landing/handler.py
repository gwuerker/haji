import tornado.web

class HomeHandler(tornado.web.RequestHandler):
	def get(self):
		self.render('templates/home.tmpl')

class UserHandler(tornado.web.RequestHandler):
	def get(self, user):
		if user:
			params = {
				'user' : user
			}
			self.render('templates/user.tmpl', **params)
		else:
			self.render('templates/users.tmpl')


