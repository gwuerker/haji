import sys
    
from tornado.options import define, options
from application import HajiLandingApplication

# Main execution
if __name__ == '__main__':
	try:
		haji_landing_application = HajiLandingApplication()
		haji_landing_application.run()
	except KeyboardInterrupt:
		sys.exit(0)
