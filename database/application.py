import logging

import tornado.ioloop
import tornado.web
import sqlite3

from handler import JobHandler

class HajiDatabaseApplication(tornado.web.Application):
	def __init__(self):
		tornado.web.Application.__init__(self)	
		self.add_handlers('', [
			(r'/([^/]*)', JobHandler),
		])
	
		db_conn = sqlite3.connect('jobs.db')
		cursor = db_conn.cursor()
		cursor.execute('''CREATE TABLE if not EXISTS jobs (
			id 			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			torque_id 		TEXT,
			user 			TEXT,
			user_group 		TEXT,
			job_name 		TEXT,
			session_id 		TEXT,
			requested_resources 	TEXT,
			used_resources 		TEXT,
			queue 			TEXT,
			exit_code 		TEXT,
			exec_host		TEXT,
			start_time		TEXT)
		''')
		db_conn.commit()
		db_conn.close()

	def run(self):
		self.listen(52030)
		tornado.ioloop.IOLoop.instance().start()


