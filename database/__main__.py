import sys
    
from tornado.options import define, options
from application import HajiDatabaseApplication

# Main execution
if __name__ == '__main__':
	try:
		haji_database_application = HajiDatabaseApplication()
		haji_database_application.run()
	except KeyboardInterrupt:
		sys.exit(0)
