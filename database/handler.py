import tornado.web
import sqlite3
import json
import subprocess

from xml.dom import minidom

class JobHandler(tornado.web.RequestHandler):
	def set_default_headers(self):
		self.set_header("Access-Control-Allow-Origin", "http://bgsc.uwec.edu:52031")	

	def post(self, value=None):
		db_conn = sqlite3.connect('jobs.db')
		cursor = db_conn.cursor()
		
		values = []
		cols = []
		for col in self.request.arguments:
			cols.append(col)
			values.append(self.get_argument(col))

		xml = subprocess.check_output('qstat -fx {}'.format(self.get_argument('torque_id')), shell=True)
		dom = minidom.parseString(xml)
		job = dom.getElementsByTagName('Job')[0]

		exec_host = job.getElementsByTagName('exec_host')[0].childNodes[0].data
		cols.append('exec_host')
		values.append(exec_host)

		start_time =  job.getElementsByTagName('start_time')[0].childNodes[0].data
		cols.append('start_time')
		values.append(start_time)
			
		cursor.execute('INSERT INTO Jobs ("{}") VALUES ("{}")'.format('","'.join(cols), '","'.join(values)))
	
		db_conn.commit()
		db_conn.close()

	def get(self, value=None):
		db_conn = sqlite3.connect('jobs.db')
		cursor = db_conn.cursor()
		
		limit = self.get_argument('limit', 25, True)

		if value:
			query = """SELECT * FROM (
					SELECT * FROM jobs 
					WHERE torque_id = "{}"  OR user = "{}" OR id = "{}" 
					ORDER BY start_time DESC LIMIT {}
				) sub
				ORDER BY start_time DESC"""
				
			cursor.execute(query.format(value, value, value, limit))
		else:
			query = """SELECT * FROM (
					SELECT * FROM jobs
					ORDER BY start_time DESC LIMIT {}
				) sub
				ORDER BY start_time DESC"""
			cursor.execute(query.format(limit))

		self.write(query_to_json(cursor))
		db_conn.close()

def query_to_json(cursor):
	rows = [x for x in cursor]
	cols = [x[0] for x in cursor.description]
	jobs = []
	for row in rows:
		job = {}
		for prop, val in zip(cols, row):
			job[prop] = val
		jobs.append(job)
	
	#clean-up resources
	
	for job in jobs:
		used_resources_parsed = {}
		for resource in job['used_resources'].split(','):
			resource_split = resource.split('=')	
			used_resources_parsed[resource_split[0]] = resource_split[1]			
		job['used_resources'] = used_resources_parsed
		
		requested_resources_parsed = {}
		for resource in job['requested_resources'].split(','):
			resource_split = resource.split('=')
			# deal with multiple "="s in requested resources string
			name = resource_split.pop(0)
			requested_resources_parsed[name] = '='.join(resource_split)	
		job['requested_resources'] = requested_resources_parsed

	return json.dumps(jobs)


